from datetime import datetime as dt
class timen:
  def __init__(self):
    pass
  @property
  def now(self):
    return dt.now()
  @property
  def nown(self):
    return dt.ctime(dt.now())
  @property
  def year(self):
    return int(dt.now().year)
  @property
  def month(self):
    return int(dt.now().month)
  @property
  def monthn(self):
    return dt.strftime(dt.now(), '%B')
  @property
  def day(self):
    return int(dt.now().day)
  @property
  def dayn(self):
    return dt.strftime(dt.now(), '%A')
  @property
  def hour(self):
    return int(dt.now().hour)
  @property
  def minute(self):
    return int(dt.now().minute)
  @property
  def second(self):
    return int(dt.now().second)
  @property
  def micros(self):
    return int(dt.now().microsecond)
  @property
  def ftime(self):
    return dt.strftime(dt.now(), '%X')
  @property
  def ispm(self):
    return False if (self.hour - 12) < 0 else True
